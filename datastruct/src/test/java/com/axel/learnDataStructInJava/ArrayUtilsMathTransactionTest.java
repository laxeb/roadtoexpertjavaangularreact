package com.axel.learnDataStructInJava;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class ArrayUtilsMathTransactionTest {

    @Test
    void shouldReturn1for123() {
        assertEquals(1, ArrayUtilsMathTransaction.findMin(new int[]{1, 2, 3}));
    }

    @Test
    void shouldReturn3for123() {
        assertEquals(3, ArrayUtilsMathTransaction.findMax(new int[]{1, 2, 3}));
    }

    @Test
    void shouldReturn12for121314(){
        assertThat(ArrayUtilsMathTransaction.findMin(new int[]{12, 22, 13, 14}), equalTo(12));
    }

    @Test
    void shouldThrownAnExceptionIfArrayIsEmpty() {
        int [] emptyArray = new int[0];
        assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtilsMathTransaction.findMin(emptyArray)) ;
    }

    @Test
    void shouldFindTheMaxValueInArray() {
        assertEquals(10, ArrayUtilsMathTransaction.findMax(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9,10, 10}));
    }

    @Test
    void shouldReturnTheIndexWhereTheSumOfTwoValuesIsEqualToTheTarget() {
        int [] numbers = new int[]{8, 2, 3, 4, 5, 10 , 13 , 55 , 60 , 11} ;
        int target = 15 ;
        int [] expected = new int[]{4 , 5};

        Assertions.assertArrayEquals(expected, ArrayUtilsMathTransaction.findSum(numbers, target));
        assertEquals(target ,numbers[expected[0]] + numbers[expected[1]] );
    }

    @Test
    void shouldReturnNullIfNoneSumOfElementIsTarget()
    {
        int [] numbers = new int[]{8, 2, 3, 4, 5, 10 , 13 , 55 , 60 , 11} ;
        int target = 200 ;
        assertNull(ArrayUtilsMathTransaction.findSum(numbers, target));
    }
}
