package com.axel.learnDataStructInJava;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ArrayUtilsMathTransaction {

     static int findMin(int[] array) {
        Arrays.sort(array);
        return array[0];
    }

     static int findMax(int[] array) {
        Arrays.sort(array);
        return array[array.length - 1];
    }

    /**
     * find if the sum  of two values is equal to the target
     * @param array the array to search
     * @param target the target value
     * @return list of indexes where the sum of two values is equal to the target
     */
     static int[] findSum(int[] array , int target ) {
        if (array == null || array.length == 0) {
            return null ;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            int complement = target - array[i];
            if (map.containsKey(complement)) {
                return new int[]{ map.get(complement) , i };
            }
            map.put(array[i], i);
        }
        return null ;
    }
}
