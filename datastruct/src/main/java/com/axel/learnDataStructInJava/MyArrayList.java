package com.axel.learnDataStructInJava;

import java.util.Arrays;
import java.util.Comparator;

public class MyArrayList<T> {
    public int size;
    public int capacity;
    private T[] data;
    public MyArrayList() {
        this.size = 0;
        this.capacity = 10;
        this.data = (T[]) new Object[10];
    }
    public MyArrayList(int size) {
        this.size = 0;
        this.capacity = size ;
        this.data = (T[]) new Object[size];
    }
    public void add(T t) {
        if (this.size == this.capacity) {
            this.capacity *= 2;
            this.data = Arrays.copyOf(this.data, this.capacity);
        }
        this.data[this.size++] = t;
    }
    public T get(int index) {
        return this.data[index];
    }

    /**
     * The array should be sorted in ascending order in order to use this method .
     * @param t the element to find in the array
     * @return the index of the element in the array, or -1 if the element is not in the array
     */
    public int find(T t, Comparator<T> comparator) {
       return  Arrays.binarySearch(this.data ,0 , this.size, t,  comparator);
    }

}
